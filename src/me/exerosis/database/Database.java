package me.exerosis.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class Database {
	protected Connection connection = null;
	protected Statement statement;
	protected String password;

	protected Database(String password) {
		this.password = password;
		openConnection(password);
	}

	public abstract boolean openConnection(String password);

	public boolean isOnline() {
		return openConnection(password);
	}

	public Connection getConnection() {
		return connection;
	}

	public Statement getStatement() {
		try {
			if(statement == null || statement.isClosed())
				statement = connection.createStatement();
		} catch (SQLException e) {}
		return statement;
	}

	protected boolean closeConnection() throws SQLException {
		if (connection == null)
			return false;
		connection.close();
		return true;
	}

	protected ResultSet querySQL(String query) throws SQLException {
		if(statement == null || statement.isClosed())
			statement = connection.createStatement();
		return statement.executeQuery(query);
	}

	protected int updateSQL(String query) throws SQLException {
		if(statement == null || statement.isClosed())
			statement = connection.createStatement();
		return statement.executeUpdate(query);
	}
}