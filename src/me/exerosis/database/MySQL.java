package me.exerosis.database;

import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL extends Database{
	private final static String USER = "Exerosis";
	private final static String DATABASE = "exerosis";
	private final static int PORT = 3306;
	private final static String HOSTNAME = "localhost";

	public MySQL(String password) {
		super(password);
	}	
	
	@Override
	public boolean openConnection(String password){
		try {
			connection = DriverManager.getConnection("jdbc:mysql://" + HOSTNAME + ":" + PORT + "/" + DATABASE, USER, password);
		} catch (SQLException e) {
			connection = null;
		}
		try {
			if(connection == null || connection.isClosed())
				return false;
		} catch (SQLException e) {}		
		return true;
	}
}
