package me.exerosis.queue.command;

public enum Type {
	UPDATE, INSERT, DELETE, TRUNCATE, REPLACE;
}
