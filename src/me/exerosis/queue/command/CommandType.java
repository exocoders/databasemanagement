package me.exerosis.queue.command;

public enum CommandType {
	QUERY, UPDATE;
}
