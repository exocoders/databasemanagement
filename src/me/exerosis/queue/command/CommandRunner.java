package me.exerosis.queue.command;

import java.io.Serializable;

import me.exerosis.database.table.SQLResult;

public interface CommandRunner extends Serializable{
	public void run(SQLResult result);
}
