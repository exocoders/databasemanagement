package me.exerosis.queue.command;

public enum CommandPriority {	
	MANDATORY, CAN_QUEUE
}
