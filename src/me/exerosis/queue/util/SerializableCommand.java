package me.exerosis.queue.util;

import java.io.Serializable;

import me.exerosis.queue.command.CommandMaker;
import me.exerosis.queue.command.CommandRunner;
import me.exerosis.queue.command.CommandType;
import me.exerosis.queue.command.SQLCommand;

public class SerializableCommand implements Serializable{
	private String command;
	private CommandType type;
	private CommandRunner runnable;
	
	public SerializableCommand(String command, CommandType type, CommandRunner runnable) {
		this.command = command;
		this.type = type;
		this.runnable = runnable;
	}
	
	public SQLCommand toSQLCommand(){
		return new SQLCommand(new CommandMaker(command), type, null, runnable);
	}
	
	public String getCommand() {
		return command;
	}
	public CommandRunner getRunnable() {
		return runnable;
	}
	public CommandType getType() {
		return type;
	}
}
