package me.exerosis.queue.util;

public enum TableState {
	SMART_QUEUING, FORCE_QUEUE, FORCE_RUN;
}
