package me.exerosis.queue;

import java.util.HashMap;
import java.util.HashSet;

import me.exerosis.SQLAPI.TableManager;
import me.exerosis.database.MySQLTable;
import me.exerosis.packet.ticker.AsyncTickListener;
import me.exerosis.packet.ticker.AsyncTicker;
import me.exerosis.queue.command.SQLCommand;

import org.bukkit.Bukkit;

public class MySQLQueue extends SQLQueue implements AsyncTickListener {
	private static final long serialVersionUID = 2030379640711426410L;

	private static HashMap<MySQLTable, MySQLQueue> queues = new HashMap<MySQLTable, MySQLQueue>();

	private MySQLTable table;
	private boolean online = true;
	private int i = 0;

	private MySQLQueue(MySQLTable table) {
		super(table.getName());
		this.table = table;
		AsyncTicker.registerListener(this);
	}

	@Override
	public void tick() {
		i ++;
		if(!(i%40 == 0))
			return;
		if(i%400 == 0)
			System.out.println("[SQL] " + size() + " commands queued! Server state: " + (table.isOnline() ? "online!" : "offline!"));
		if(!(size() > 0))
			return;
		if(!table.isOnline()){
			online = false;
			return;
		}
		if(!online){
			online = true;
			System.out.println("[SQL] The server is online again, loading from backed up queues and pushing all!");
			return;
		}

		System.out.println("[SQL] Trying to push command!");
		if(!executeFirst())
			System.out.println("[SQL] Failed to push command!");
		else
			System.out.println("[SQL] Pushed command!");
	}
	
	/**
	 * Forces this queue to try to push all queued commands to the SQL server.
	 */
	public void pushToDatabase(){
		HashSet<SQLCommand> allCommands = new HashSet<SQLCommand>();
		allCommands.addAll(this);
		for(SQLCommand command: allCommands){
			table.runCommand(command);
			remove(command);
		}
		allCommands.clear();
		if(size() > 0)
			saveToArchive();
	}

	private boolean executeFirst(){
		SQLCommand command = firstElement();
		remove(command);
		if(table.runCommand(command))
			return true;
		return false;
	}

	public static MySQLQueue getTableQueue(String tableName){
		return getTableQueue(TableManager.getTable(tableName));
	}

	public static MySQLQueue getTableQueue(MySQLTable table){
		if(queues.containsKey(table))
			return queues.get(table);
		MySQLQueue queue = new MySQLQueue(table);
		queues.put(table, queue);
		return queue;
	}
}
