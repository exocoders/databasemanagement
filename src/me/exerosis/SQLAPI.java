package me.exerosis;

import java.util.HashMap;

import me.exerosis.database.MySQLTable;
import me.exerosis.queue.command.CommandMaker;
import me.exerosis.queue.command.CommandPriority;
import me.exerosis.queue.command.CommandRunner;
import me.exerosis.queue.command.CommandType;
import me.exerosis.queue.command.SQLCommand;

import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class SQLAPI extends JavaPlugin implements Listener{
	private static Plugin plugin;
	private MySQLTable table;
	private static boolean debug;

	@Override
	public void onEnable() {
		plugin = this;
	}
	
	public static Plugin getPlugin(){
		return plugin;
	}
	public static void setDebugMode(boolean debug) {
		SQLAPI.debug = debug;
	}
	public static boolean isDebugMode() {
		return debug;
	}
	
	public static class TableManager {
		private static HashMap<String, MySQLTable> tables = new HashMap<String, MySQLTable>();
		
		public static MySQLTable addTable(String name, String password){
			tables.put(name, new MySQLTable(password, name));
			return tables.get(name); 
		}
		public static MySQLTable getTable(String name){
			return tables.get(name);
		}
		public static void executeCommand(String name, SQLCommand command) {
			getTable(name).executeCommand(command);
		}
	}
	
	public static class CommandManager {		
		public static SQLCommand createCommand(CommandMaker command, CommandType type, CommandPriority priority, CommandRunner runnable){
			return new SQLCommand(command, type, priority, runnable);
		}
	}
	
}



/*
		table = TableManager.getTable("test");
	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("table")) {

			if(args.length == 1 && args[0].equals("clear")){
				SQLCommand command = new SQLCommand("TRUNCATE TABLE  test", CommandType.QUERY, CommandPriority.CAN_QUEUE, new Command("Cleared!"));
				table.executeCommand(command);

				System.out.println("[SQL] Clear table command queued!");
			}

			else if(args.length == 1 && args[0].equals("ask")){
				SQLCommand command = new SQLCommand("SELECT * FROM test", CommandType.QUERY, CommandPriority.CAN_QUEUE, new Command("Asked!"));
				table.executeCommand(command);

				System.out.println("[SQL] Query command queued!");
			}

			else if(args.length == 3 && args[0].equals("put")){
				SQLCommand command = new SQLCommand("INSERT INTO test (testInt, testIntTwo) VALUES ('" + args[1] + "', '" + args[2] + "')", CommandType.UPDATE, CommandPriority.CAN_QUEUE,  new Command("Added"));
				table.executeCommand(command);

				System.out.println("[SQL] Add command queued!");
			}

			return true;
		} 
		return false; 
	}


	public MySQLTable getTable() {
		return table;
	}
}


class Command implements CommandRunner {
	private static final long serialVersionUID = 5569616250202530053L;
	String msg;
	public Command(String msg) {
		this.msg = msg;
	}

	@Override
	public void run(SQLResult result) {
		System.out.println(msg);

	}
*/	